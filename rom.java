//Das programm Liest ein römischer Zahl ein und macht ein Dezimal daraus

import java.util.Scanner;
import java.lang.String;

public class rom {


    //Methode zum umwandeln der einzeln Character werte
    public static int addition(String zahl_röm) {
        int[] zahl = new int[zahl_röm.length()];
        int summe = 0;

        for (int i = 0; i < zahl_röm.length(); i++) {

            //1 oder -1
            if (zahl_röm.charAt(i) == 'I') {
                if (((i+1) < zahl_röm.length()) && ((zahl_röm.charAt(i+1) == 'X') || (zahl_röm.charAt(i+1) == 'V')))
                    zahl[i] = -1;
                else zahl[i] = 1;
            }
            //5
            else if (zahl_röm.charAt(i) == 'V')
                zahl[i] = 5;

                //10 oder -10
            else if (zahl_röm.charAt(i) == 'X')
                if (((i+1) < zahl_röm.length()) && ((zahl_röm.charAt(i+1) == 'L') || (zahl_röm.charAt(i+1) == 'C')))
                    zahl[i] = -10;
                else zahl[i] = 10;

                //50
            else if (zahl_röm.charAt(i) == 'L')
                zahl[i] = 50;

                //100 oder -100
            else if (zahl_röm.charAt(i) == 'C')
                if (((i+1) < zahl_röm.length()) && ((zahl_röm.charAt(i+1) == 'D') || (zahl_röm.charAt(i+1) == 'M')))
                    zahl[i] = -100;
                else zahl[i] = 100;

                //500
            else if (zahl_röm.charAt(i) == 'D')
                zahl[i] = 500;

                //1000
            else if (zahl_röm.charAt(i) == 'M')
                zahl[i] = 1000;
            summe = summe + zahl[i];
        }

        return summe;
    }



    //Methode zum prüfen der Zahl
    public static boolean check(String röm_zahl) {
        boolean stat = true;
        if (röm_zahl.contains("XXXX") || röm_zahl.contains("XXXXX") || röm_zahl.contains("XXXXXX")
                || röm_zahl.contains("IIII") || röm_zahl.contains ("IIIII") || röm_zahl.contains("IIIIII")
                || röm_zahl.contains("CCCC") || röm_zahl.contains("CCCCC") || röm_zahl.contains("CCCCCC")
                || röm_zahl.contains("MMMM") || röm_zahl.contains("MMMMM") || röm_zahl.contains("MMMMMM")
                || röm_zahl.contains("VV")
                || röm_zahl.contains("LL")
                || röm_zahl.contains("DD"))
            stat = false;
        return stat;
    }



    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Bitte geben die Zahl ein:");
        String zahl_röm = myScanner.next();

        while(check(zahl_röm)){
            System.out.println(addition(zahl_röm));
            break;
        }
    }
}


